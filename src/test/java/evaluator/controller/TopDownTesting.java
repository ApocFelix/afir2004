package evaluator.controller;

import evaluator.util.InputValidationTest;
import org.junit.Test;

/**
 * Created by SESIN on 22-May-18.
 */
public class TopDownTesting {
    InputValidationTest testA = new InputValidationTest();
    AppControllerTest testBC = new AppControllerTest();

    @Test
    public void TCA() throws Exception{
        try{
            testA.TC1();
            testA.TC2();
            assert true;
        }
        catch(Exception ex){
            assert false;
        }
    }

    @Test
    public void TCAB() throws Exception{
        try{
            testA.TC1();
            testA.TC2();
            testBC.TC1();
            testBC.TC2();
            testBC.TC3();
            assert true;
        }
        catch(Exception ex){
            assert false;
        }
    }

    @Test
    public void TCABC() throws Exception{
        try{
            testA.TC1();
            testA.TC2();
            testBC.TC1();
            testBC.TC2();
            testBC.TC3();
            testBC.TC4();
            testBC.TC5();
            assert true;
        }
        catch(Exception ex){
            assert false;
        }
    }
}
