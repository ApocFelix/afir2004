package evaluator.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by SESIN on 22-May-18.
 */
public class StartGuiApp extends Application {
    private FXMLLoader loader;
    private Stage primaryStage;
    private AnchorPane rootLayout1;

    public static void main(String[] args){
        launch(args);
    }

    public void start(Stage primaryStage){
        try {
            this.primaryStage = primaryStage;
            loader = new FXMLLoader();
            String pathToFxml = "src/main/java/evaluator/gui/GuiAdd.fxml";
            URL fxmlUrl = new File(pathToFxml).toURI().toURL();
            loader.setLocation(fxmlUrl);
            GuiAdd guiAdd = new GuiAdd();
            loader.setController(guiAdd);
            rootLayout1 = loader.load();
            Scene scene = new Scene(rootLayout1);
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }
}
