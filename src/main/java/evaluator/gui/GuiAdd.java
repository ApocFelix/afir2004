package evaluator.gui;

import evaluator.controller.AppController;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by SESIN on 22-May-18.
 */
public class GuiAdd implements Initializable{
    @FXML
    private TextField textEnunt;
    @FXML
    private TextField textRaspuns1;
    @FXML
    private TextField textRaspuns2;
    @FXML
    private TextField textRaspuns3;
    @FXML
    private TextField textRaspunsCorect;
    @FXML
    private TextField textDomeniu;
    @FXML
    private TableView<Intrebare> tableviewAdd;
    @FXML
    private TableColumn<Intrebare, String> colEnunt;
    @FXML
    private TableColumn<Intrebare, String> colRasp1;
    @FXML
    private TableColumn<Intrebare, String> colRasp2;
    @FXML
    private TableColumn<Intrebare, String> colRasp3;
    @FXML
    private TableColumn<Intrebare, String> colRaspCor;
    @FXML
    private TableColumn<Intrebare, String> colDomeniu;
    @FXML
    private Button btnAdd;

    private static final String file = "C:\\Users\\SESIN\\Desktop\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\evaluator\\intrebari.txt";
    private List<Intrebare> lista = new ArrayList<>();
    private AppController appController = new AppController();


    public GuiAdd(){
        appController.loadIntrebariFromFile(file);
        lista = appController.getIntrebari();}
    public void initialize(URL location, ResourceBundle resources){
        try{
            colEnunt.setCellValueFactory(new PropertyValueFactory<Intrebare, String>("enunt"));
            colRasp1.setCellValueFactory(new PropertyValueFactory<Intrebare, String>("varianta1"));
            colRasp2.setCellValueFactory(new PropertyValueFactory<Intrebare, String>("varianta2"));
            colRasp3.setCellValueFactory(new PropertyValueFactory<Intrebare, String>("varianta3"));
            colRaspCor.setCellValueFactory(new PropertyValueFactory<Intrebare, String>("variantaCorecta"));
            colDomeniu.setCellValueFactory(new PropertyValueFactory<Intrebare, String>("domeniu"));

            final ObservableList<Intrebare> data = FXCollections.observableArrayList(lista);
            tableviewAdd.setItems(data);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void refresh(){
        final ObservableList<Intrebare> data = FXCollections.observableArrayList(lista);
        tableviewAdd.setItems(data);
    }

    @FXML
    public void setBtnAdd(){
        btnAdd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String enunt = textEnunt.getText();
                String raspuns1 = textRaspuns1.getText();
                String raspuns2 = textRaspuns2.getText();
                String raspuns3 = textRaspuns3.getText();
                String raspunsCorect = textRaspunsCorect.getText();
                String domeniu = textDomeniu.getText();
                if (enunt.isEmpty() || raspuns1.isEmpty() || raspuns2.isEmpty() || raspuns3.isEmpty() || raspunsCorect.isEmpty() || domeniu.isEmpty()){
                    showErrorMessage("niciuna dintre casute n-ar trebui sa ramana goale");
                }
                else{
                    try{
                        lista.add(new Intrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, domeniu));
                        refresh();
                    }
                    catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    private static void showMessage(Alert.AlertType type, String header, String text)
    {
        Alert message=new Alert(type);
        message.setHeaderText(header);
        message.setContentText(text);
        message.showAndWait();
    }

    private static void showErrorMessage(String text)
    {
        Alert message=new Alert(Alert.AlertType.ERROR);
        message.setTitle("Mesaj eroare");
        message.setContentText(text);
        message.showAndWait();
    }
}
